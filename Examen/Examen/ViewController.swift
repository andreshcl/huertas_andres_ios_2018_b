//
//  ViewController.swift
//  Examen
//
//  Created by Andres Huertas on 27/11/18.
//  Copyright © 2018 Andres Huertas. All rights reserved.
//

import UIKit
import FirebaseAuth

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    @IBOutlet weak var userTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    
    @IBAction func enterButton(_ sender: Any) {
        let username = userTextField.text!
        let password = passwordTextField.text!
        
        Auth.auth().signIn(withEmail: username, password: password) { (data, error) in
            if let error = error {
                print(error)
                return
            }
           
            self.performSegue(withIdentifier: "login", sender: self )
        }

        
    }
    
}

