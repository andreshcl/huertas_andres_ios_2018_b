//
//  ViewControllerLogged.swift
//  Examen
//
//  Created by Andres Huertas on 27/11/18.
//  Copyright © 2018 Andres Huertas. All rights reserved.
//

import UIKit

class ViewControllerLogged: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBOutlet weak var numberTextField: UITextField!
    
    
    @IBOutlet weak var numbersLabel: UILabel!
    
    
    @IBAction func getNumbersButton(_ sender: Any) {
        let n:Int = Int(numberTextField.text!)!
        
        print(n)
        var cad = ""
        for i in 0..<n{
            cad += String(i*2)
            cad += " "
            self.numbersLabel.text = cad
            
        }
    }


    @IBAction func exitButton(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    
}
